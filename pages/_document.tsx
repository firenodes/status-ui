import Document, { Html, Head, Main, NextScript, DocumentContext } from "next/document"
import React, { useEffect } from "react"
import { initTheme } from "../lib/theme"

class MyDocument extends Document {
    static async getInitialProps(ctx: DocumentContext) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html>
                <Head />
                <body className="dark:bg-gray-800 dark:text-white bg-gray-500">
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default MyDocument
