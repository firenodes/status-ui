export const getTheme = () => localStorage.getItem("theme") !== "dark"

export const initTheme = () => {
    if (
        getTheme() ||
        (!("theme" in localStorage) && window.matchMedia("(prefers-color-scheme: dark)").matches)
    )
        document.documentElement.classList.add("dark")
    else document.documentElement.classList.remove("dark")
}

export const setThemeTo = (dark: boolean) => {
    localStorage.setItem("theme", dark ? "dark" : "light")
    initTheme()
}

export const setTheme = () => setThemeTo(getTheme())
