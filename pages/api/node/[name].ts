// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next"
import { fetchInfo } from "../../../lib/node"
import * as dt from "date-fns"

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    // res.status(200).json({ name: "John Doe" })
    const config = await import("../../../config.json")
    const { name } = req.query

    const configNode = config.nodes.find((n) => n.name === name)
    if (!configNode) return res.status(404).json({ message: `Node not found: ${name}` })
    const infoRes = await fetchInfo(configNode, {
        fromDate: new Date(req.query.from as string),
        toDate: new Date(req.query.to as string),
    })

    if (infoRes && infoRes.data) res.json({ node: { data: infoRes.data, name: configNode.name } })
}
