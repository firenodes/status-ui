import { faCaretDown } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React, { PropsWithChildren, ReactNode, useState } from "react"

export const Dropdown = (
    props: PropsWithChildren<{ buttonText: ReactNode; buttonIcon?: ReactNode }>
) => {
    const [enabled, setEnabled] = useState(false)

    return (
        <div>
            <button
                className="block p-2 w-28 rounded-full overflow-hidden border-2 border-gray-600 focus:outline-none focus:border-white"
                onClick={() => setEnabled(!enabled)}
            >
                {props.buttonText}
                {props.buttonIcon || (
                    <FontAwesomeIcon
                        className="ml-2"
                        icon={faCaretDown}
                        transform={{ rotate: enabled ? 0 : -90 }}
                    />
                )}
            </button>
            <DropdownList enabled={enabled}>{props.children}</DropdownList>
        </div>
    )
}

export const DropdownList = (props: PropsWithChildren<{ enabled: boolean }>) =>
    props.enabled ? (
        <div className="absolute z-20 mt-2 py-2 w-48 dark:bg-gray-700 bg-gray-50 rounded-lg shadow-xl">
            {props.children}
        </div>
    ) : (
        <div />
    )

export const DropdownItem = (props: PropsWithChildren<Record<string, unknown>>) => (
    <div className="block px-4 py-2 text-gray-800 hover:bg-gray-700 hover:text-white dark:text-white dark:bg-gray-800 dark:hover:bg-gray-900">
        {props.children}
    </div>
)
