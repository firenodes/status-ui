import { useEffect, useState } from "react"
import { Node } from "../components/node"
import { Info, InfoList } from "../lib/node"
import axios from "axios"

export default function Home() {
    const [nodes, setNodes] = useState<InfoList[]>([])

    useEffect(() => {
        axios.get<{ nodes: InfoList[] }>("/api/nodes").then((res) => {
            if (res.status === 200) setNodes(res.data.nodes)
        })
        const i = setInterval(() => {
            axios.get<{ nodes: InfoList[] }>("/api/nodes").then((res) => {
                if (res.status === 200) setNodes(res.data.nodes)
            })
        }, 15000)

        return () => {
            clearInterval(i)
        }
    }, [])

    return (
        <div className="container mx-auto p-3">
            <div>
                {nodes.length < 1 && (
                    <div>
                        <h2 className="2xl">No nodes found.</h2>
                    </div>
                )}
                {nodes.map((node) => node && <Node key={node.name} node={node} />)}
            </div>
        </div>
    )
}
