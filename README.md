# Status UI

## Documentation

### Prerequisites

- Node.js
- Yarn Package Manager
- A server with an agent installed

### Configuration

Create a config.json file containing the servers you want to monitor:

```json
{
    "nodes": [
        {
            "name": "Server 1",
            "address": "http://YOUR_AGENT_IP:8000"
        }
    ]
}
```

### Building and Running

```bash
yarn run build
yarn run start
```

### Developing With Live Reload

```bash
yarn run dev
```

## Agent

In order to collect node data such as cpu usage, you need to have [an agent installed](https://gitlab.com/firenodes/status-agent) on the server you want to collect statistics from.
