import React, { useEffect } from "react"
import "tailwindcss/tailwind.css"
import { initTheme } from "../lib/theme"
import { Header } from "./header"

const MyApp = ({ Component, pageProps }) => {
    useEffect(() => {
        initTheme()
    }, [])

    return (
        <div>
            <Header />
            <Component {...pageProps} />
        </div>
    )
}

export default MyApp
