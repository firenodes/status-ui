import axios from "axios"

export interface Info {
    status: string
    timestamp: string
    cpuUsage: number
    ramUsage: number
}

export interface InfoList {
    name: string
    data: Info[]
}

export const fetchInfo = async (
    n: { name: string; address: string },
    query: { fromDate: Date; toDate: Date }
) => {
    // Check if the node status is ok
    // console.log(`from=${query.fromDate.toISOString()}&to=${query.toDate.toISOString()}`)
    try {
        return await axios.get<Info[]>(
            `${
                n.address
            }/data/between?from=${query.fromDate.toISOString()}&to=${query.toDate.toISOString()}`
        )
    } catch (err) {
        console.error(`Failed to fetch data from ${n.address}: ${err}`)
    }
}

export const average = (nums: number[]) => nums.reduce((sum, elem) => sum + elem, 0) / nums.length
export const between = (x: number, min: number, max: number) => x >= min && x <= max
