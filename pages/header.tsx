import { faLightbulb } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import React from "react"
// import { Dropdown, DropdownItem } from "../components/dropdown"
import { setTheme } from "../lib/theme"
import Link from "next/link"

export const Header = () => {
    return (
        <div className="header bg-gray-700 dark:bg-gray-900 text-white w-100 p-4 flex flex-row mb-4">
            <div className="header-left-nav flex justify-center items-center ml-5">
                <div aria-label="Branding" className="mr-5">
                    <h1 className="text-4xl font-bold">
                        <Link href="/">FireNodes Status</Link>
                    </h1>
                </div>
            </div>
            <div className="header-right-nav ml-auto mr-5">
                <button onClick={setTheme}>
                    <FontAwesomeIcon size="4x" icon={faLightbulb} className="text-yellow-300" />
                </button>
            </div>
        </div>
    )
}
