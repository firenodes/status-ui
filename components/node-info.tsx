import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCloud, faMemory, faMicrochip } from "@fortawesome/free-solid-svg-icons"
import { InfoList } from "../lib/node"
import React, { PropsWithChildren, useEffect, useState } from "react"
import { UsageChart } from "./chart"

export const NodeInfo = (props: PropsWithChildren<{ node: InfoList }>) => {
    return (
        <div className="mx-auto bg-gray-400 dark:bg-gray-700 rounded-lg flex flex-col justify-start items-center mb-4 p-8 min-w-full">
            <h3 className="mb-4">{props.node.name}</h3>
            {/* <FontAwesomeIcon
                icon={faCloud}
                size="1x"
                className={`${props.data.status ? "text-green-600" : "text-red-600"} mb-2`}
            /> */}
            {props.children}
            {props.node.data && props.node.data.length > 0 ? (
                <div>
                    <div className="flex flex-col justify-start items-center mb-4">
                        <div className="flex flex-row justify-start items-center mb-4">
                            <p className="mr-4">CPU %</p>
                            <FontAwesomeIcon icon={faMicrochip} size="1x" />
                        </div>

                        <UsageChart
                            data={props.node.data.map((d) => ({
                                key: new Date(d.timestamp),
                                data: d.cpuUsage,
                            }))}
                        />
                    </div>
                    <div className="flex flex-col justify-start items-center mb-4">
                        <div className="flex flex-row justify-start items-center mb-4">
                            <p className="mr-4">RAM %</p>
                            <FontAwesomeIcon icon={faMemory} size="1x" />
                        </div>

                        <UsageChart
                            data={props.node.data.map((d) => ({
                                key: new Date(d.timestamp),
                                data: parseInt(d.ramUsage.toFixed(2)),
                            }))}
                        />
                    </div>
                </div>
            ) : (
                <p>No data found for {props.node.name}.</p>
            )}
        </div>
    )
}
