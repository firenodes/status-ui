import React from "react"
import {
    ChartInternalDataTypes,
    DiscreteLegend,
    DiscreteLegendEntry,
    LinearXAxis,
    LineChart,
    LineSeries,
    PointSeries,
} from "reaviz"
import { average, between } from "../lib/node"

export const UsageChart = (props: { data: { key: ChartInternalDataTypes; data: number }[] }) => {
    return (
        <LineChart
            width={600}
            height={300}
            data={props.data}
            series={
                <LineSeries
                    colorScheme={(data: { value: number }[]) => {
                        const mean = average(data.map((d) => d.value))
                        return between(mean, 0, 49)
                            ? "#3cc761"
                            : between(mean, 50, 70)
                            ? "#e8d543"
                            : "#e87743"
                    }}
                    symbols={<PointSeries show="hover" />}
                />
            }
            xAxis={<LinearXAxis type="time" />}
        >
            <DiscreteLegend entries={[<DiscreteLegendEntry label="CPU Usage" />]} />
        </LineChart>
    )
}
