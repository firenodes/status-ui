// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next"
import { fetchInfo, Info, InfoList } from "../../lib/node"
import * as dt from "date-fns"

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    const config = await import("../../config.json")

    const result: InfoList[] = []

    for await (const configNode of config.nodes) {
        const infoRes = await fetchInfo(configNode, {
            fromDate: dt.subDays(new Date(), 5),
            toDate: dt.addDays(new Date(), 5),
        })

        if (infoRes && infoRes.data) result.push({ data: infoRes.data, name: configNode.name })
    }

    res.status(200).json({ nodes: result })
}
