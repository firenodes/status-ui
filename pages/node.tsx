import React, { useEffect, useState } from "react"
import { Info, InfoList } from "../lib/node"
import axios from "axios"
import { NodeInfo } from "../components/node-info"
import { useRouter } from "next/router"
import "react-datepicker/dist/react-datepicker.css"
import DatePicker from "react-datepicker"
import * as dt from "date-fns"

export default function Home() {
    const [node, setNode] = useState<InfoList | undefined>(undefined)
    const router = useRouter()
    const { name } = router.query
    const [startDate, setStartDate] = useState(dt.subHours(new Date(), 3))
    const [endDate, setEndDate] = useState(dt.addHours(new Date(), 3))

    useEffect(() => {
        if (name) {
            axios
                .get<{ node: InfoList }>(
                    `/api/node/${encodeURIComponent(
                        name.toString()
                    )}?from=${startDate.toISOString()}&to=${endDate.toISOString()}`
                )
                .then((res) => {
                    if (res.status === 200) setNode(res.data.node)
                })
            const i = setInterval(() => {
                axios
                    .get<{ node: InfoList }>(
                        `/api/node/${encodeURIComponent(
                            name.toString()
                        )}?from=${startDate.toISOString()}&to=${endDate.toISOString()}`
                    )
                    .then((res) => {
                        if (res.status === 200) setNode(res.data.node)
                    })
            }, 5000)
            return () => {
                clearInterval(i)
            }
        }
    }, [name, startDate, endDate])

    return (
        <div className="container mx-auto">
            {node ? (
                <div className="flex flex-col justify-start items-center">
                    <NodeInfo key={node.name} node={node}>
                        <div className="flex flex-col justify-start items-center mb-4">
                            {/* Date pickers */}
                            <div className="flex flex-col justify-start items-center mb-2">
                                <h3 className="text-xl mb-2">Start 🗓️</h3>
                                <DatePicker
                                    selected={startDate}
                                    onChange={(d) => setStartDate(d as Date)}
                                    showTimeSelect
                                    className="text-white bg-gray-900"
                                />
                            </div>
                            <div className="flex flex-col justify-start items-center mb-2">
                                <h3 className="text-xl mb-2">End 🗓️ </h3>
                                <DatePicker
                                    selected={endDate}
                                    onChange={(d) => setEndDate(d as Date)}
                                    showTimeSelect
                                    className="text-white bg-gray-900"
                                />
                            </div>
                        </div>
                    </NodeInfo>
                </div>
            ) : (
                <div className="flex flex-col justify-start items-center">
                    <h2 className="text-2xl font-semibold">
                        {(node && node.name) || "Unknown Node"}
                    </h2>
                    <p>This node is not reachable.</p>
                </div>
            )}
        </div>
    )
}
