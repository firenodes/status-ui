import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCloud } from "@fortawesome/free-solid-svg-icons"
import { Info, InfoList } from "../lib/node"
import { useRouter } from "next/router"

export const Node = (props: { node: InfoList }) => {
    const router = useRouter()

    return (
        <div
            className={`mx-auto dark:text-white bg-gray-400 dark:bg-gray-700 rounded-lg flex flex-row justify-center items-center mb-4 h-12 p-8 ${
                props.node.data[0]?.status
                    ? "bg-gray-400 dark:bg-gray-700"
                    : "bg-gray-500 dark:bg-gray-900"
            }`}
        >
            <button
                onClick={() => router.push({ pathname: "/node", query: { name: props.node.name } })}
                disabled={props.node.data[0]?.status && props.node.data[0]?.status !== "ok"}
                className="w-full"
            >
                <h3 className="mr-3">{props.node.name}</h3>
                <FontAwesomeIcon
                    icon={faCloud}
                    size="1x"
                    className={props.node.data[0]?.status ? "text-green-600" : "text-red-600"}
                />
            </button>
        </div>
    )
}
